﻿namespace DataBankThreat
{
	class Threat
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Source { get; set; }
		public string ObjectInfluenced { get; set; }
		public bool PrivacyViolation { get; set; }
		public bool IntegrityViolation { get; set; }
		public bool AvailabilityViolation { get; set; }

		public Threat(int id, string name, string descr, string source, string objInfluenced, 
			bool privacyViol, bool integrityViol, bool availabilityViol)
		{
			ID = id;
			Name = name;
			Description = descr;
			Source = source;
			ObjectInfluenced = objInfluenced;
			PrivacyViolation = privacyViol;
			IntegrityViolation = integrityViol;
			AvailabilityViolation = availabilityViol;
		}
	}
}
