﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;

namespace DataBankThreat
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		List<Threat> threats = new List<Threat>();
		const int countThreatsOnOnePage = 20;
		int countPages = 0;
		int currentPage = 1;

		public MainWindow()
		{
			InitializeComponent();
			buttonPrevious.IsEnabled = false;
			if (!File.Exists("threats.txt"))
			{
				MessageBoxResult mbr = MessageBox.Show("Локальной базы данных угроз ещё не существует. " +
					"Потребуется загрузить её с сайта.", "Файл не найден", MessageBoxButton.OKCancel,
					MessageBoxImage.Error);
				if (mbr == MessageBoxResult.OK)
				{
					DownloadExcel();
					ExtractFromExcel();
					SaveThreatsInLocalDB();
					ShowThreats();
				}
				else Environment.Exit(0);
			}
			else
			{
				GetThreatsFromLocalDB();
				ShowThreats();
			}
		}

		public void ClearThreats()
		{
			int j = 0;
			for (int i = 0; i < 20; i++)
			{
				((TextBlock)gridThreats.Children[j]).Text = "";
				((TextBlock)gridThreats.Children[j + 2]).Text = "";
				j += 4;
			}
		}

		public void ShowThreats()
		{
			countPages = (threats.Count / countThreatsOnOnePage) + 1;
			int j = 0;
			int endCounter = (currentPage * countThreatsOnOnePage > threats.Count) ? threats.Count : currentPage * countThreatsOnOnePage;
			for (int i = (currentPage - 1) * countThreatsOnOnePage; i < endCounter; i++)
			{
				((TextBlock)gridThreats.Children[j]).Text = "УБИ. " + string.Format("{0:d3}", 
					threats[i].ID);
				((TextBlock)gridThreats.Children[j+2]).Text = threats[i].Name;
				j += 4;
			}
		}

		public void DownloadExcel()
		{
			using (WebClient wc = new WebClient())
			{
				wc.DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", "thrlist.xlsx");
			}
		}

		public void ExtractFromExcel()
		{
			Excel.Application excel = new Excel.Application();
			Excel.Workbook excelWB = null;
			string pathToFile = System.IO.Path.GetFullPath("thrlist.xlsx").Replace('\\', '/');
			threats = new List<Threat>();
			try
			{
				excelWB = excel.Workbooks.Open(pathToFile);
				Excel.Worksheet excelWS = excelWB.ActiveSheet;
				int i = 3;
				while (excelWS.Cells[i, 1].Value != null)
				{
					int id = int.Parse(excelWS.Cells[i, 1].Value.ToString());
					string name = excelWS.Cells[i, 2].Value.ToString();
					string descr = excelWS.Cells[i, 3].Value.ToString();
					string source = excelWS.Cells[i, 4].Value.ToString();
					string objInfluenced = excelWS.Cells[i, 5].Value.ToString();
					bool privacyViol = (excelWS.Cells[i, 6].Value.ToString() == "1") ? true : false;
					bool integrityViol = (excelWS.Cells[i, 7].Value.ToString() == "1") ? true : false;
					bool availabilityViol = (excelWS.Cells[i, 8].Value.ToString() == "1") ? true : false;
					Threat threat = new Threat(id, name, descr, source, objInfluenced, privacyViol, integrityViol,
						availabilityViol);
					threats.Add(threat);
					i++;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show("Exception!!! " + e);
			}
			finally
			{
				excelWB.Close();
				excel.Quit();
			}
		}

		public void SaveThreatsInLocalDB()
		{
			FileStream fs = new FileStream("threats.txt", FileMode.Create);
			using (StreamWriter sw = new StreamWriter(fs, Encoding.GetEncoding(1251)))
			{
				for (int i = 0; i < threats.Count; i++)
				{
					string str = "{" + string.Format("id: \"{0}\", name: \"{1}\", descr: \"{2}\", " +
						"source: \"{3}\", objInfluenced: \"{4}\", privacyViol: \"{5}\", integrityViol: " +
						"\"{6}\", availabilityViol: \"{7}\"",
						threats[i].ID, threats[i].Name, threats[i].Description, threats[i].Source,
						threats[i].ObjectInfluenced, threats[i].PrivacyViolation, threats[i].IntegrityViolation,
						threats[i].AvailabilityViolation) + "}";
					sw.WriteLine(str);
				}
			}
		}

		public void GetThreatsFromLocalDB()
		{
			using (StreamReader sr = new StreamReader("threats.txt", Encoding.GetEncoding(1251)))
			{
				string str = sr.ReadToEnd();
				string[] threatsArray = str.Split('{');
				for (int i = 1; i < threatsArray.Length; i++)
				{
					int id = int.Parse(threatsArray[i].Split(new[] { "id: \"" }, StringSplitOptions.None)[1]
						.Split(new[] { "\", name: \"" }, StringSplitOptions.None)[0]);
					string name = threatsArray[i].Split(new[] { "name: \"" }, StringSplitOptions.None)[1]
						.Split(new[] { "\", descr: \"" }, StringSplitOptions.None)[0];
					string descr = threatsArray[i].Split(new[] { "descr: \"" }, StringSplitOptions.None)[1]
						.Split(new[] { "\", source: \"" }, StringSplitOptions.None)[0];
					string source = threatsArray[i].Split(new[] { "source: \"" }, StringSplitOptions.None)[1]
						.Split(new[] { "\", objInfluenced: \"" }, StringSplitOptions.None)[0];
					string objInfluenced = threatsArray[i].Split(new[] { "objInfluenced: \"" }, 
						StringSplitOptions.None)[1].Split(new[] { "\", privacyViol: \"" }, StringSplitOptions.None)[0];
					bool privacyViol = bool.Parse(threatsArray[i].Split(new[] { "privacyViol: \"" }, 
						StringSplitOptions.None)[1].Split(new[] { "\", integrityViol: \"" }, StringSplitOptions.None)[0]);
					bool integrityViol = bool.Parse(threatsArray[i].Split(new[] { "integrityViol: \"" }, 
						StringSplitOptions.None)[1].Split(new[] { "\", availabilityViol: \"" }, StringSplitOptions.None)[0]);
					bool availabilityViol = bool.Parse(threatsArray[i].Split(new[] { "availabilityViol: \"" }, 
						StringSplitOptions.None)[1].Split(new[] { "\"}" }, StringSplitOptions.None)[0]);
					Threat threat = new Threat(id, name, descr, source, objInfluenced, privacyViol, integrityViol,
						availabilityViol);
					threats.Add(threat);
				}
			}
		}

		public void UpdateThreats()
		{
			List<Threat> threatsOld = threats;
			DownloadExcel();
			ExtractFromExcel();
			int countUpdateThreats = 0;
			string updated = "";
			List<string> threatsUpdate = new List<string>();
			for (int i = 0; i < threatsOld.Count; i++)
			{
				if (threats[i].ID == threatsOld[i].ID && threats[i].Name != threatsOld[i].Name ||
					threats[i].Description != threatsOld[i].Description ||
					threats[i].Source != threatsOld[i].Source ||
					threats[i].ObjectInfluenced != threatsOld[i].ObjectInfluenced ||
					threats[i].PrivacyViolation != threatsOld[i].PrivacyViolation ||
					threats[i].IntegrityViolation != threatsOld[i].IntegrityViolation ||
					threats[i].AvailabilityViolation != threatsOld[i].AvailabilityViolation)
				{
					countUpdateThreats++;
					updated += countUpdateThreats + ". Обновлена угроза УБИ. " + string.Format("{0:d3}", threatsOld[i].ID) + "\n";
					if (threats[i].Name != threatsOld[i].Name)
						updated += threatsOld[i].Name + " -> " + threats[i].Name + "\n";
					if (threats[i].Description != threatsOld[i].Description)
						updated += threatsOld[i].Description + " -> " + threats[i].Description + "\n";
					if (threats[i].Source != threatsOld[i].Source)
						updated += threatsOld[i].Source + " -> " + threats[i].Source + "\n";
					if (threats[i].ObjectInfluenced != threatsOld[i].ObjectInfluenced)
						updated += threatsOld[i].ObjectInfluenced + " -> " + threats[i].ObjectInfluenced + "\n";
					if (threats[i].PrivacyViolation != threatsOld[i].PrivacyViolation)
						updated += threatsOld[i].PrivacyViolation + " -> " + threats[i].PrivacyViolation + "\n";
					if (threats[i].IntegrityViolation != threatsOld[i].IntegrityViolation)
						updated += threatsOld[i].IntegrityViolation + " -> " + threats[i].IntegrityViolation + "\n";
					if (threats[i].AvailabilityViolation != threatsOld[i].AvailabilityViolation)
						updated += threatsOld[i].AvailabilityViolation + " -> " + threats[i].AvailabilityViolation + "\n";
					threatsUpdate.Add(updated);
				}
			}
			if (threats.Count > threatsOld.Count)
			{
				for (int i = threatsOld.Count; i < threats.Count; i++)
				{
					countUpdateThreats++;
					updated += countUpdateThreats + ". Добавлена угроза УБИ. " + string.Format("{0:d3}", threatsOld[i].ID) + "\n";
					updated += "Наименование угрозы: " + threats[i].Name + "\n";
					updated += "Описание угрозы: " + threats[i].Description + "\n";
					updated += "Источник угрозы: " + threats[i].Source + "\n";
					updated += "Объект воздействия угрозы: " + threats[i].ObjectInfluenced + "\n";
					updated += "Нарушение конфиденциальности: " + threats[i].PrivacyViolation + "\n";
					updated += "Нарушение целостности: " + threats[i].IntegrityViolation + "\n";
					updated += "Нарушение доступности: " + threats[i].AvailabilityViolation + "\n";
					threatsUpdate.Add(updated);
				}
			}
			string statusUpdateText = "Статус обновления: Успешно\n";
			string countUpdateThreatsText = "Общее количество обновленных записей: " + countUpdateThreats + "\n";
			string fullUpdated = statusUpdateText + countUpdateThreatsText;
			foreach (string str in threatsUpdate)
			{
				fullUpdated += str;
			}
			informationBlock.Text = fullUpdated;
		}

		private void Button_Update(object sender, RoutedEventArgs e)
		{
			//gridThreats.IsEnabled = false;

			UpdateThreats();
			SaveThreatsInLocalDB();
			currentPage = 1;
			labelCurrentPage.Content = currentPage.ToString();
			buttonNext.IsEnabled = true;
			buttonPrevious.IsEnabled = false;
			ClearThreats();
			ShowThreats();
			MessageBox.Show("Данные обновлены", "Обновление", MessageBoxButton.OK, MessageBoxImage.Information);
		}

		private void Button_Save(object sender, RoutedEventArgs e)
		{
			SaveThreatsInLocalDB();
			MessageBox.Show("Данные записаны в файл threats.txt", "Сохранение", MessageBoxButton.OK, MessageBoxImage.Information);
		}

		private void Button_Previous(object sender, RoutedEventArgs e)
		{
			currentPage--;
			ClearThreats();
			ShowThreats();
			labelCurrentPage.Content = currentPage.ToString();
			if (!buttonNext.IsEnabled) buttonNext.IsEnabled = true;
			if (currentPage == 1) buttonPrevious.IsEnabled = false;
		}

		private void Button_Next(object sender, RoutedEventArgs e)
		{
			currentPage++;
			ClearThreats();
			ShowThreats();
			labelCurrentPage.Content = currentPage.ToString();
			if (!buttonPrevious.IsEnabled) buttonPrevious.IsEnabled = true;
			if (currentPage == countPages) buttonNext.IsEnabled = false;
		}

		private void ShowOneThreat(object sender, RoutedEventArgs e)
		{
			int indexThreat = int.Parse(((TextBlock)sender).Text.Substring(5)) - 1;
			string fullCurrentthreat = "";
			fullCurrentthreat += "Идентификатор угрозы: УБИ. " + string.Format("{0:d3}", threats[indexThreat].ID) + "\n\n";
			fullCurrentthreat += "Наименование угрозы: " + threats[indexThreat].Name + "\n\n";
			fullCurrentthreat += "Описание угрозы: " + threats[indexThreat].Description + "\n\n";
			fullCurrentthreat += "Источник угрозы: " + threats[indexThreat].Source + "\n\n";
			fullCurrentthreat += "Объект воздействия угрозы: " + threats[indexThreat].ObjectInfluenced + "\n\n";
			fullCurrentthreat += "Нарушение конфиденциальности: ";
			if (threats[indexThreat].PrivacyViolation) fullCurrentthreat += "1\n\n";
			else fullCurrentthreat += "0\n\n";
			fullCurrentthreat += "Нарушение целостности: ";
			if (threats[indexThreat].IntegrityViolation) fullCurrentthreat += "1\n\n";
			else fullCurrentthreat += "0\n\n";
			fullCurrentthreat += "Нарушение доступности: ";
			if (threats[indexThreat].AvailabilityViolation) fullCurrentthreat += "1\n";
			else fullCurrentthreat += "0\n";
			informationBlock.Text = fullCurrentthreat;
		}
	}
}
